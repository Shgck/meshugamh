import os.path

import fbx
from FbxCommon import InitializeSdkObjects, LoadScene

from pywmo import Wmo, WmoGroup
from batch_gen import generate_batches
from bsp_gen import generate_bsp_trees
import pywmo_utilities


class FbxImporter(object):
    """ FBX importer. Creates the binding with the FBX SDK,
        import an FBX and convert it to a WMO. """
        
    mapping_modes = [ "None", "By Control Point", "By Polygon Vertex",
                      "By Polygon", "By Edge", "All Same" ]
    reference_modes = [ "Direct", "Index", "Index to Direct" ]
    
    def __init__(self, config):
        self.manager, self.scene = InitializeSdkObjects()
        self.scene_is_ok = False
        self.config = config

    def __del__(self):
        self.manager.Destroy()


    def load_file(self, file_path):
        """ Load the scene of a FBX file. """
        self.scene_is_ok = LoadScene(self.manager, self.scene, file_path)
            

    def create_wmo(self):
        """ Creates a WMO from the scene loaded earlier.
            Returns None if failed at some point. """
                    
        wmo = Wmo()
        scene_root_node = self.scene.GetRootNode()
        
        # Ambient color
        wmo.ambient_color = fbxcolor_tuple(
            self.scene.GlobalLightSettings().GetAmbientColor())
        
        # Materials
        try:
            self._import_materials(wmo)
        except FbxImportError as e:
            print("ERROR : {}".format(e.message))
            return None
        
        # Scene children
        for c in range(scene_root_node.GetChildCount()):
            child_node = scene_root_node.GetChild(c)
            attr_type = child_node.GetNodeAttribute().GetAttributeType()
            
            # Groups (= meshes)
            if attr_type == fbx.FbxNodeAttribute.eMesh:
                try:
                    self._import_mesh(child_node, wmo)
                except MappingModeError as e:
                    print("ERROR (mapping mode) : {}".format(e.message))
                    return None
                except FbxImportError as e:
                    print("ERROR : {}".format(e.message))
                    return None
        
        pywmo_utilities.sort_polygons_by_mat(wmo)
        
        # DEBUG dummy material
        # not sure why i added this... 
#         wmo.materials.append( { "flags": 0,
#                                 "shader": 0,
#                                 "blend_mode": 0,
#                                 "texture1": None,
#                                 "ambient_color": (0, 0, 0, 0),
#                                 "flags_1": 0,
#                                 "texture2": None,
#                                 "diffuse_color": (0, 0, 0, 0),
#                                 "flags_2": 0,
#                                 "unknown1": 0,
#                                 "unknown2": 0,
#                                 "unknown3": 0,
#                                 "name": "kesta" } )
#         wmo.num_materials += 1
                
        # If a material couldn't link to a texture during mesh parsing,
        # we add a default one
        fallback_texture_offsets = (-1, -1) 
        for mat in wmo.materials:
            if mat["texture1"] is None:
                print(mat["name"], "can't link to a texture, setting fallback.")
                
                # If the fallback texture hasn't been added yet, do it
                if fallback_texture_offsets[0] == -1:
                    available_offset1 = pywmo_utilities.get_next_texture_offset(wmo.textures)
                    wmo.textures.append( (
                        available_offset1,
                        self.config["fallback_texture"],
                        "__fallback" )
                    )
                    
                    available_offset2 = pywmo_utilities.get_next_texture_offset(wmo.textures)
                    wmo.textures.append( (
                        available_offset2,
                        "",
                        "__filler" )
                    )
                    
                    fallback_texture_offsets = (available_offset1, available_offset2)
                
                mat["texture1"] = fallback_texture_offsets[0]
                mat["texture2"] = fallback_texture_offsets[1]

        # Handle in/outdoor configuration according to the config file settings.
        for i, g in enumerate(wmo.groups):
            
            # Automatic default feature : everything outdoor
            wmo.group_infos[i]["flags"] |= 0x8
            g.flags |= 0x8 
        
        # redo this
#         groups_to_modify = []
#         if global_conf.get("prompt_indoor_groups") == "yes":
#             # Ask for the groups to set indoor
#             groups_to_modify = input("Enter which groups you want indoor "
#                 "(e.g. \"0 1 2 5 6\") : ").split()
#             
#         elif global_conf.get("prompt_indoor_groups") == "auto":
#             # Grabs the "auto_indoor_groups" config line to indoor groups
#             if global_conf.get("auto_indoor_groups"):
#                 groups_to_modify = global_conf["auto_indoor_groups"].split()
#             else:
#                 print("Error : asked for automatic indoor groups, "
#                        "but no auto_indoor_groups entry given.")
# 
#         try:
#             # Now that we maybe have a string list containing groups ID
#             # we can safely remove outdoor flag (as every group has it there)
#             # and add the indoor flag.
#             for str_i in list(set(groups_to_modify)):
#                 i = int(str_i)
#                 wmo.group_infos[i]["flags"] -= 0x8
#                 wmo.group_infos[i]["flags"] |= 0x2000
#                 wmo.groups[i].flags -= 0x8
#                 wmo.groups[i].flags |= 0x2000
#         except IndexError:
#             pass

        # Final generations
        pywmo_utilities.compute_global_bbox(wmo)
        generate_batches(wmo)
        generate_bsp_trees(wmo, self.config)
        
        wmo.doodad_sets.append( {
            "name": "Set_$DefaultGlobal\x00\x00",
            "first_def": 0,
            "num_defs": 0,
            "unknown": 0
        } )
        wmo.num_sets += 1
        
#         pywmo_utilities.display_materials(wmo)
#         pywmo_utilities.display_textures(wmo)

        return wmo


    def _import_mesh(self, node, wmo):
        """ Import the mesh represented by the node in the wmo. 
        
            Imports vertices, faces, the bounding box,
            and if available, normals, uv mapping, vertex colors, material ids.
            Writes the group name in wmo.group_names.
            Creates an entry in wmo.group_infos. 
        """
        group = WmoGroup()

        print("Importing mesh {}...".format(node.GetName()))
        
        group_name_offset = pywmo_utilities.get_next_group_name_offset(wmo.group_names)
        wmo.group_names.append( (group_name_offset, node.GetName()) )
        group.name_offset = group_name_offset
        
        mesh = node.GetNodeAttribute()

        if not mesh.IsTriangleMesh():
            raise FbxImportError("this mesh contains not-triangle polygons.")

        # Import bounding boxes
        mesh.ComputeBBox()
        group.bbox1 = fbxvec3_tuple(mesh.BBoxMin.Get())
        group.bbox2 = fbxvec3_tuple(mesh.BBoxMax.Get())
        
        # DEBUG : I need to avoid having multiple for UV for the same CP.
        # This will sadly duplicate most of the CP tho.
        print("#CP before splitting :", mesh.GetControlPointsCount())
        mesh.SplitPoints(fbx.FbxLayerElement.eTextureDiffuse)
        print("#CP after splitting :", mesh.GetControlPointsCount())

        # Import vertices + normals in the layers
        group.vertices = [None] * mesh.GetControlPointsCount()
        group.normals = [None] * len(group.vertices)

        # ComputeVertexNormals() is now deprecated and removed from Python bindings,
        # so I'm adding the new function WITHOUT TESTING because I want a binary release quick.
        # Edit: it seems to work well with simple models.
        mesh.GenerateNormals(True, True, False)
        
        for v, fbxvec in enumerate(mesh.GetControlPoints()):
            group.vertices[v] = fbxvec3_tuple(fbxvec)
            
            for l in range(mesh.GetLayerCount()):
                le_normals = mesh.GetLayer(l).GetNormals()
                if le_normals:
                    if le_normals.GetMappingMode() == fbx.FbxLayerElement.eByControlPoint:
                        if le_normals.GetReferenceMode() == fbx.FbxLayerElement.eDirect:
                            group.normals[v] = fbxvec3_tuple(le_normals.GetDirectArray().GetAt(v))
                        else:
                            raise FbxImportError("Wrong normal reference mode.")
                    else:
                        raise MappingModeError("Normals aren't stored by vertex :",
                            FbxImporter.mapping_modes[le_normals.GetMappingMode()])

        # Import faces + UV / colors
        num_polygons = mesh.GetPolygonCount()
        group.faces = [None] * num_polygons
        group.texture_maps = [None] * len(group.vertices)
        group.vertex_colors = [None] * len(group.vertices)
        
        polygon_list = mesh.GetPolygonVertices()
        
        # DEBUG
        def display_uv(uv):
            print("Coords :", uv[0], "&", uv[1])
        
        # vertex_id is a running ID for by-polygon-vertex mapping modes 
        vertex_id = 0
        for p in range(num_polygons):
            
            # Import face
            group.faces[p] = (
                polygon_list[p * 3],
                polygon_list[p * 3 + 1],
                polygon_list[p * 3 + 2]
            )
            
            for pv in [0, 1, 2]:
                # For each polygon vertex, get the corresponding CP ID
                control_point_id = mesh.GetPolygonVertex(p, pv)
                
                # Mapping/reference modes happy fuckfest incoming
                for l in range(mesh.GetLayerCount()):
                    
                    # UV parsing. If incorrect mapping mode, raise an error
                    # because it's a mandatory setting for WMOs.
                    le_uv = mesh.GetLayer(l).GetUVs()
                    if le_uv:
                        
                        if le_uv.GetMappingMode() == fbx.FbxLayerElement.eByControlPoint:
                            
                            if le_uv.GetReferenceMode() == fbx.FbxLayerElement.eDirect:
                                group.texture_maps[control_point_id] = fbxvec2_to_uv(
                                    le_uv.GetDirectArray().GetAt(control_point_id) )
                                
                            elif le_uv.GetReferenceMode() == fbx.FbxLayerElement.eIndexToDirect:
                                uv_id = le_uv.GetIndexArray().GetAt(control_point_id)
                                group.texture_maps[control_point_id] = fbxvec2_to_uv(
                                    le_uv.GetDirectArray().GetAt(uv_id) )
                        
                        # TODO is this fully working ? -> with the split, seems so
                        elif le_uv.GetMappingMode() == fbx.FbxLayerElement.eByPolygonVertex:
                            
                            if group.texture_maps[control_point_id] == None:
                                uv_id = mesh.GetTextureUVIndex(p, pv)
    
                                if le_uv.GetReferenceMode() == fbx.FbxLayerElement.eDirect or \
                                   le_uv.GetReferenceMode() == fbx.FbxLayerElement.eIndexToDirect:
                                    group.texture_maps[control_point_id] = fbxvec2_to_uv(
                                        le_uv.GetDirectArray().GetAt(uv_id) )
                            # DEBUG
                            else:
#                                 print("CP wasn't None, skipping")
                                pass
                                
                        else:
                            raise FbxImportError("UV mapping doesn't make sense")
                    
                    # Vertex colors parsing. No errors when there is an incorrect
                    # mapping mode, just a warning.
                    le_vtxc = mesh.GetLayer(l).GetVertexColors()
                    if le_vtxc:
                        if le_vtxc.GetMappingMode() == fbx.FbxLayerElement.eByControlPoint:
                            if le_vtxc.GetReferenceMode() == fbx.FbxLayerElement.eDirect:
                                group.vertex_colors[control_point_id] = fbxcolor_tuple(
                                    le_vtxc.GetDirectArray().GetAt(control_point_id) )
                            elif le_vtxc.GetReferenceMode() == fbx.FbxLayerElement.eIndexToDirect:
                                vtxc_id = le_vtxc.GetIndexArray().GetAt(control_point_id)
                                group.vertex_colors[control_point_id] = fbxcolor_tuple(
                                    le_vtxc.GetDirectArray().GetAt(vtxc_id) )
                        
                        # TODO test this, not sure about group.vtx index
                        elif le_vtxc.GetMappingMode() == fbx.FbxLayerElement.eByPolygonVertex:
                            if le_vtxc.GetReferenceMode() == fbx.FbxLayerElement.eDirect:
                                group.vertex_colors[control_point_id] = fbxcolor_tuple(
                                    le_vtxc.GetDirectArray().GetAt(vertex_id) )
                            elif le_vtxc.GetReferenceMode() == fbx.FbxLayerElement.eIndexToDirect:
                                vtxc_id = le_vtxc.GetIndexArray().GetAt(vertex_id)
                                group.vertex_colors[control_point_id] = fbxcolor_tuple(
                                    le_vtxc.GetDirectArray().GetAt(vtxc_id) )

                        else:
                            print("Vertex colors detected but aren't set by "
                                  "control point nor by polygon vertex !")
                    
                # end of layer loop
                vertex_id += 1
            # end of 3-vertex loop
        # end of polygon loop
        
        # if no vertex colors has been seen, delete the array
        # else set the MOCV flag
        if None in group.vertex_colors:
            group.vertex_colors = None
        else:
            group.flags |= 0x4
                        
        # Find per mesh information about material
        # There probably is a better way to do this but hey, what about doing a
        # SDK without a single explanation about how using it, except outdated
        # deprecated example code ?
        
        # Annexe function for setting correct texture ID (texture1/2) 
        def set_texture_id(texture, material):
            for mat in wmo.materials:
                if mat["name"] == material.GetName():
                    
                    # if that texture is already registered, pass
                    if mat["texture1"] is not None:
                        pass
                    # else, register the texture in MOTX (+ dummy) and set the id
                    else:
                        available_offset = pywmo_utilities.get_next_texture_offset(wmo.textures)
                        # get clear texture name (without dir + BLP ext)
                        blp_file_name = os.path.splitext(
                            os.path.split(texture.GetFileName())[1] )[0] + ".blp"
                        
                        # if texture path prefix available, set it
                        if self.config["textures_path"]:
                            blp_file_name = self.config["textures_path"] + blp_file_name

                        wmo.textures.append( (
                            available_offset,
                            blp_file_name,
                            texture.GetName()  # set a texture name for further testing
                        ) )
                        mat["texture1"] = available_offset
                        
                        available_offset = pywmo_utilities.get_next_texture_offset(wmo.textures)
                        wmo.textures.append( (
                            available_offset,
                            "",
                            "__filler"
                        ) )
                        mat["texture2"] = available_offset
                    break
                
            else:
                raise FbxImportError("Tried to link a texture to a material that doesn't exist.")
        
        for m in range(mesh.GetNode().GetSrcObjectCount(fbx.FbxSurfaceMaterial.ClassId)):
            material = mesh.GetNode().GetSrcObject(fbx.FbxSurfaceMaterial.ClassId, m)
            if material:
                for texture_id in range(fbx.FbxLayerElement.sTypeTextureCount()):
                    texture_prop = material.FindProperty(
                        fbx.FbxLayerElement.sTextureChannelNames(texture_id) )
                    if texture_prop.IsValid():
                        for t in range(texture_prop.GetSrcObjectCount(fbx.FbxTexture.ClassId)):
                            texture = texture_prop.GetSrcObject(fbx.FbxTexture.ClassId, t)
                            if texture:
                                set_texture_id(texture, material)
                                
        # Import polygon infos (dummy flags + material indices)
        # This has to be done after material import because of the poorly
        # understood material relative linking system. 
        group.polygon_infos = [(0, 0)] * num_polygons

        for l in range(mesh.GetLayerCount()):
            le_materials = mesh.GetLayer(l).GetMaterials()
            
            if le_materials:
                if le_materials.GetMappingMode() != fbx.FbxLayerElement.eByPolygon:
                    raise MappingModeError("Materials aren't mapped by polygons :",
                        FbxImporter.mapping_modes[le_materials.GetMappingMode()])

                # Creates a binding between the relative ID and
                # the wmo.materials ID, using materials names
                material_id_links = [None] * node.GetMaterialCount()
                for mat_id in range(node.GetMaterialCount()):
                    material = node.GetMaterial(mat_id)
                    for i, mat in enumerate(wmo.materials):
                        if mat["name"] == material.GetName():
                            material_id_links[mat_id] = i
                            break
                    else:
                        raise FbxImportError("Mesh {} requires unknown "
                            "material {}".format(node.GetName()), material.GetName())
                
                # Now, set the good flags / material ID in MOPY
                for m in range(le_materials.GetIndexArray().GetCount()):
                    group.polygon_infos[m] = (
                        0x20,
                        material_id_links[le_materials.GetIndexArray().GetAt(m)] 
                    )
                      
                # I don't want to deal with other material layer, for now
                break
        
        # Adds group and group_info in wmo. No flags are added !
        wmo.group_infos.append( {
            "flags": 0,
            "bbox1": group.bbox1,
            "bbox2": group.bbox2,
            "name_index": group_name_offset
        } )
        wmo.groups.append(group)
        wmo.num_groups += 1

    
    def _import_materials(self, wmo):
        """ Import materials in the WMO.
        
            Loads name, ambient/diffuse colors, else it's dummy information.
            Textures indices are loaded per mesh for some reason.
            Returns nothing. """
        
        for i in range(self.scene.GetSrcObjectCount()):
            obj = self.scene.GetSrcObject(i)
             
            if obj.ClassId.GetFbxFileTypeName() != "Material":
                continue
                
            # So... with this parsing method, materials can get listed more
            # than once, so let's not flood our WMO.
            mat_already_there = False
            for mat in wmo.materials:
                if mat["name"] == obj.GetName():
                    mat_already_there = True
                    break
            if mat_already_there:
                continue
            
            material = {}
            material["name"] = obj.GetName()
            if material["name"].endswith("_tr"):
                material["blend_mode"] = 1
            material["texture1"] = None
            material["texture2"] = None
            
            # Get ambient/diffuse colors
            ambient_color = obj.FindProperty("AmbientColor")
            diffuse_color = obj.FindProperty("DiffuseColor")
             
            if ambient_color.IsValid():
                ambient_color = fbx.FbxPropertyDouble3(ambient_color).Get()
                material["ambient_color"] = ( int(ambient_color[2] * 255)
                                            , int(ambient_color[1] * 255)
                                            , int(ambient_color[0] * 255)
                                            , 0xFF )
            else:
                material["ambient_color"] = (0, 0, 0, 255)
                
            if diffuse_color.IsValid():
                diffuse_color = fbx.FbxPropertyDouble3(diffuse_color).Get()
                material["diffuse_color"] = ( int(diffuse_color[2] * 255)
                                            , int(diffuse_color[1] * 255)
                                            , int(diffuse_color[0] * 255)
                                            , 0xFF )
            else:
                material["diffuse_color"] = (0, 0, 0, 255)
            
            # Misc parameters necessary for pywmo
            material["flags"] = 0
            material["shader"] = 0
            material["blend_mode"] = 0
            material["flags_1"] = 0
            material["flags_2"] = 0
            material["unknown1"] = 0
            material["unknown2"] = 0
            material["unknown3"] = 0
             
            wmo.materials.append(material)
            wmo.num_materials += 1
                 
        # end of source objects parsing
        

    def _display_info(self):
        """ Display basic informations for quick debugging. """
        def display_node_rec(node, indent):
            print(" "*indent, node.GetName(), node)
            for i in range(node.GetChildCount()):
                display_node_rec(node.GetChild(i), indent + 4)
        
        def display_props(obj):
            print(obj.ClassId.GetFbxFileTypeName(), obj.GetName())
        
        root_node = self.scene.GetRootNode()
        print("SCENE CHILDREN :", root_node.GetChildCount())
        for i in range(root_node.GetChildCount()):
            display_node_rec(root_node.GetChild(i), 4)
            
        print("SCENE SOURCE OBJECTS :", self.scene.GetSrcObjectCount())
        for i in range(self.scene.GetSrcObjectCount()):
            display_props(self.scene.GetSrcObject(i))
    
    
    @staticmethod
    def _display_layer_info(layer):
        """ Display mapping and reference mode for this layer. """
        
        print("Layer {} : mapping {}, referencing {}".format(
              layer.GetName(),
              FbxImporter.mapping_modes[layer.GetMappingMode()],
              FbxImporter.reference_modes[layer.GetReferenceMode()] ))





class FbxImportError(Exception):
    """ Generic error for FBX import. """

    def __init__(self, message):
        self.message = message


class MappingModeError(Exception):
    """ FBX import error : I guess the only mapping mode supported for WMO
        is by control point (= by vertex). Is there a way to convert it ?
        Maybe but for now, it's just considered invalid and raise this.
        Edit : nah I can actually handle it, this exception is a bit dumb... """

    def __init__(self, message):
        self.message = message





def fbxvec2_to_uv(fbxvec):
    """ Return (u,1-v) """
    return (fbxvec[0], 1.0 - fbxvec[1])

def fbxvec3_tuple(fbxvec):
    """ Return (x,y,z) """
    return (fbxvec[0], fbxvec[1], fbxvec[2])

def fbxcolor_tuple(fbxcolor):
    """ Return (b,g,r,a) with 0,255 colors """
    return ( int(fbxcolor.mBlue * 255)
           , int(fbxcolor.mGreen * 255)
           , int(fbxcolor.mRed * 255)
           , int(fbxcolor.mAlpha * 255) )
