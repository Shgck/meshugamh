""" Generates BSP trees.

    Relax, it's just trees.
    http://vektroid.bandcamp.com/track/om-namo-ocean-road
"""

from random import choice
from pywmo_utilities import get_empty_bsp_node


def generate_bsp_trees(wmo, config):
    """ Create a BSP tree for every mesh of the WMO. """
    print("Generating BSP trees...")
    gen = BspTreeGenerator(config)
    for g in wmo.groups:
        gen.generate_tree(g)
        g.flags |= 0x1  # Has MOBN and MOBR chunk.


def generate_fake_tree(group):
    """ Generate fake single-leafed BSP tree. """
    
    # fake leaf
    group.bsp_nodes = [ {
        "type": "leaf",
        "child1": -1,
        "child2": -1,
        "num_faces": len(group.faces),
        "first_face": 0,
        "f_dist": 0
    } ]
    
    # complete ref index
    group.bsp_refs = list(range(len(group.faces)))


#------------------------------------------------------------------------------


class BoundingBox(object):
    """ Represents a box in space, by two vertex corners.
    
        This is used by the generate_tree function to handle easily the
        splitting process and that jazz.
    """
    
    def __init__(self, inf_corner, sup_corner):
        self.inf_corner = inf_corner
        self.sup_corner = sup_corner
        
    
    
    def contains_vertex(self, v):
        """ Detects if the box contains the vertex v. """
        return (v[0] >= self.inf_corner[0] and v[0] <= self.sup_corner[0]) and \
                (v[1] >= self.inf_corner[1] and v[1] <= self.sup_corner[1]) and \
                (v[2] >= self.inf_corner[2] and v[2] <= self.sup_corner[2])
        
    def contains_polygon(self, v1, v2, v3):
        """ Detects if the box contains the polygon p (if at least one of its
            vertices is in the box) """
        return self.contains_vertex(v1) or \
                self.contains_vertex(v2) or \
                self.contains_vertex(v3)
                
                
                
    def split(self, plane_type):
        """ Split the box in two other boxes of equal space, using the plane
            described by plane_type.
            
            plane_type must be "YZ", "XZ" or "XY".
            
            Calculates f_dist as the absolute average value (absolute as
            absolute in space, not as "always positive") then
            returns (BBox1, BBox2, f_dist).
        """
        
        bbox1 = BoundingBox(self.inf_corner, self.sup_corner)
        bbox2 = BoundingBox(self.inf_corner, self.sup_corner)
        average = 0
        
        if plane_type == "YZ":
            average = round((self.sup_corner[0] + self.inf_corner[0]) / 2)
            bbox1.sup_corner = (average, bbox1.sup_corner[1], bbox1.sup_corner[2])
            bbox2.inf_corner = (average, bbox2.inf_corner[1], bbox2.inf_corner[2])
            
        elif plane_type == "XZ":
            average = round((self.sup_corner[1] + self.inf_corner[1]) / 2)
            bbox1.sup_corner = (bbox1.sup_corner[0], average, bbox1.sup_corner[2])
            bbox2.inf_corner = (bbox2.inf_corner[0], average, bbox2.inf_corner[2])
            
        elif plane_type == "XY":
            average = round((self.sup_corner[2] + self.inf_corner[2]) / 2)
            bbox1.sup_corner = (bbox1.sup_corner[0], bbox1.sup_corner[1], average)
            bbox2.inf_corner = (bbox2.inf_corner[0], bbox2.inf_corner[1], average)
        
        return bbox1, bbox2, average


class BspTreeGenerator(object):
    """ Generator, wrapped in a class to keep config, in case of. """
    
    def __init__(self, config):
        self.max_leaves = config["bsp_leaf_max_size"]
    

    def generate_tree(self, group):
        """ Generate a correct BSP tree, considered ok but in testing. """
        
        # Build a root bbox / node
        root_bbox = BoundingBox(group.bbox1, group.bbox2)
        root_node = get_empty_bsp_node()
        root_node["refs"] = list(range(len(group.faces)))
        
        # Start recursive creation of nodes
        group.bsp_nodes = [root_node]
        self.process_node(group, root_bbox, group.bsp_nodes[0])
        
        # Create the BSP references list
        group.bsp_refs = []
        for node in group.bsp_nodes:
            if node["type"] == "leaf":
                group.bsp_refs += node["refs"]
                
        # Set corrects num_faces and first_face 
        face_count = 0
        for node in group.bsp_nodes:
            if node["type"] == "leaf":
                node["num_faces"] = len(node["refs"])
                node["first_face"] = face_count
                face_count += node["num_faces"]


    def process_node(self, group, bbox, node, method = None):
        """ Process a BSP node to build its child and all.
        
            The optional method arg is when I need to force a splitting method.
            Best way I've found so far...
        """
        
        def get_random_split_method(old_method):
            """ Annexe function to get a random splitting method in case of
                infinite loop, that is not the same as old_method.
                See below for explanations. """
            new_method = "YZ"
            while new_method == old_method:
                new_method = choice( ("YZ", "XZ", "XY") )
            return new_method
    
        if len(node["refs"]) <= self.max_leaves:
            node["type"] = "leaf"
            return
        
        # Get the bboxes and children nodes
        if method is None:
            bbox_child1, bbox_child2, node_child1, node_child2, split_method = \
                    self.create_balanced_children(group, bbox, node)
        else:
            bbox_child1, bbox_child2, node_child1, node_child2, split_method = \
                    self.create_children(group, bbox, node, method)
                
        if split_method == "YZ":
            node["type"] = "YZ"
        elif split_method == "XZ":
            node["type"] = "XZ"
        elif split_method == "XY":
            node["type"] = "XY"
    
        # Now we recursively handle the two children.
        # child2 before child1, for some reason.
    
        # If a child node has no refs, there is no meaning in storing it so its
        # father index is set as -1.
        if node_child2["refs"]:
            node["child2"] = len(group.bsp_nodes)
            group.bsp_nodes.append(node_child2)
            # As this algo sucks a bit, it may happen that the splitting box is
            # reduced to a plane, and in some cases makes the algo infinitely loop.
            # It can be detected when a child has the same number
            # of refs as its father.
            if len(node_child2["refs"]) == len(node["refs"]):
                self.process_node( group, bbox_child2, group.bsp_nodes[-1],
                                   get_random_split_method(node_child2["type"]) )
            # Else it's all good, add the child to the group list and process it.
            else:
                self.process_node(group, bbox_child2, group.bsp_nodes[-1])
        else:
            node["child2"] = -1
            
        # Does the same for child1.
        if node_child1["refs"]:
            node["child1"] = len(group.bsp_nodes)
            group.bsp_nodes.append(node_child1)
            if len(node_child1["refs"]) == len(node["refs"]):
                self.process_node( group, bbox_child1, group.bsp_nodes[-1],
                                   get_random_split_method(node_child1["type"]) )
            else:
                self.process_node(group, bbox_child1, group.bsp_nodes[-1])
        else:
            node["child1"] = -1
    
    
    def create_balanced_children(self, group, bbox, node):
        """ Creates 2 children for the node, choosing the splitting plane type
            with the least difference between the number of refs of both children.
            
            Return (bbox_child1, bbox_child2, child_node1, child_node2, method)
            The children nodes has their reference list updated,
            the father node also has a correct f_dist.
        """
        
        best_method = "YZ"
        
        # Creates YZ split bboxes and children nodes
        yz_split_bbox1, yz_split_bbox2, yz_f_dist = bbox.split("YZ")
        yz_child1, yz_child2 = (get_empty_bsp_node(), get_empty_bsp_node())
        self.fill_node_refs(group, yz_split_bbox1, node["refs"], yz_child1)
        self.fill_node_refs(group, yz_split_bbox2, node["refs"], yz_child2)
        # Take the balance as the lower as it's the first balance we have.
        lower_balance = abs( len(yz_child1["refs"]) - len(yz_child2["refs"]) )
        
        # Creates XZ split bboxes and children nodes
        xz_split_bbox1, xz_split_bbox2, xz_f_dist = bbox.split("XZ")
        xz_child1, xz_child2 = (get_empty_bsp_node(), get_empty_bsp_node())
        self.fill_node_refs(group, xz_split_bbox1, node["refs"], xz_child1)
        self.fill_node_refs(group, xz_split_bbox2, node["refs"], xz_child2)
        # Take the balance and compares it with previous lower_balance
        balance = abs( len(xz_child1["refs"]) - len(xz_child2["refs"]) )
        if balance < lower_balance:
            lower_balance = balance
            best_method = "XZ"
    
        # Creates XY split bboxes and children nodes
        xy_split_bbox1, xy_split_bbox2, xy_f_dist = bbox.split("XY")
        xy_child1, xy_child2 = (get_empty_bsp_node(), get_empty_bsp_node())
        self.fill_node_refs(group, xy_split_bbox1, node["refs"], xy_child1)
        self.fill_node_refs(group, xy_split_bbox2, node["refs"], xy_child2)
        # Take the balance and compares it with previous lower_balance
        # (no need to update balance here)
        balance = abs( len(xy_child1["refs"]) - len(xy_child2["refs"]) )
        if balance < lower_balance:
            best_method = "XY"
        
        # Now return the more balanced children
        if best_method == "YZ":
            node["f_dist"] = yz_f_dist
            return (yz_split_bbox1, yz_split_bbox2, yz_child1, yz_child2, best_method)
        elif best_method == "XZ":
            node["f_dist"] = xz_f_dist
            return (xz_split_bbox1, xz_split_bbox2, xz_child1, xz_child2, best_method)
        elif best_method == "XY":
            node["f_dist"] = xy_f_dist
            return (xy_split_bbox1, xy_split_bbox2, xy_child1, xy_child2, best_method)
    
    
    def create_children(self, group, bbox, node, method):
        """ Same as create_balanced_children, but forced to use
            a specific splitting method.
            
            Used when an infinite looped is spotted.
            Return same as function above.
        """
        split_bbox1, split_bbox2, node["f_dist"] = bbox.split(method)
        child1, child2 = (get_empty_bsp_node(), get_empty_bsp_node())
        self.fill_node_refs(group, split_bbox1, node["refs"], child1)
        self.fill_node_refs(group, split_bbox2, node["refs"], child2)
        
        return (split_bbox1, split_bbox2, child1, child2, method)
            
    
    def fill_node_refs(self, group, bbox, refs, node):
        """ Fill node["refs"] with every face reference that points to a polygon
            which is contained in the bbox. """
        
        node["refs"] = []
        for i in range(len(refs)):
            f = group.faces[refs[i]]
            if bbox.contains_polygon( group.vertices[f[0]]
                                    , group.vertices[f[1]]
                                    , group.vertices[f[2]] ):
                node["refs"].append(refs[i])



