""" EXPERIMENTAL FEATURE """

from fbx import *
from FbxCommon import InitializeSdkObjects, SaveScene

from pywmo import Wmo, WmoGroup


class FbxExporter(object):
    """  """
    
    def __init__(self, config):
        self.manager, self.scene = InitializeSdkObjects()
        self.scene_is_ok = False
        
        self.config = config

    def __del__(self):
        self.manager.Destroy()
        
        
    def save_file(self, file_path):
        """ Save the created WMO in a FBX file. """
        SaveScene(self.manager, self.scene, file_path)
        
    
    def build_fbx(self, file_path):
        """ Creates the FBX from a WMO stored at file_path. """
        
        # Global information (optional I guess)
        scene_info = FbxDocumentInfo.Create(self.manager, "SceneInfo")
        scene_info.mComment = "Exported with Meshugamh from " + file_path;
        self.scene.SetSceneInfo(scene_info)
        
        # Set WMO axis system (same as 3ds Max)
        FbxAxisSystem.Max.ConvertScene(self.scene)
        
        wmo = Wmo(file_path)
        
        for g in wmo.groups:
            self._add_mesh(g)
            
        self.scene_is_ok = True
        

    def _add_mesh(self, group):
        """ Convert WMO group to a FBX mesh. """
        mesh = FbxMesh.Create(self.manager, "mesh")
        num_vertices = len(group.vertices)
        num_polygons = len(group.faces)
        
        # Import the control points
        mesh.InitControlPoints(num_vertices)
        mesh.InitNormals(num_vertices)
        for i in range(num_vertices):
            mesh.SetControlPointAt( get_fbxvec4( group.vertices[i] )
                                  , get_fbxvec4( group.normals[i] )
                                  , i )
            
        # Import the polygons
        for p in range(num_polygons):
            mesh.BeginPolygon()
            for pv in [0, 1, 2]:
                mesh.AddPolygon(group.faces[p][pv])
            mesh.EndPolygon()

        # Finally, inserts a new node with the mesh in our scene
        mesh_node = FbxNode.Create(self.manager, "mesh_node")
        mesh_node.SetNodeAttribute(mesh)
        self.scene.GetRootNode().AddChild(mesh_node)
        
        
        
#------------------------------------------------------------------------------



class FbxExportError(Exception):
    """ Generic error for FBX export. """

    def __init__(self, message):
        self.message = message





def get_fbxvec4(vec):
    """ Returns a FBX formatted vector4 from a tuple of size 3 or more. """
    fbxvec4 = FbxVector4(vec[0], vec[1], vec[2])
    if len(vec) > 3:
        fbxvec4[3] = vec[3]
    return fbxvec4

