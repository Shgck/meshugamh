from cx_Freeze import setup, Executable

executables = [Executable('main.py')]

setup( name='Meshugamh',
       version='0.0.x',
       description='FBX to WMO converter',
       executables=executables
     )
