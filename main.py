#!/usr/bin/python
# -*-coding:utf-8 -*

#######################################################################
# Meshugamh - converter from FBX to WMO/v17
# Copyright (C) 2013 - Shgck
#  
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License or
# any later version, or version 2 if explicit permission is given.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#      
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################################

""" Converter from FBX to WMO/v17 files.
    Version and usage just below.
    
    I hope you'll have fun using this :)
"""

import os.path
import sys
import random

from config import load_config
from fbx_import import FbxImporter
from fbx_export import FbxExporter
from pywmo_utilities import semantic_check

PROG_TITLE = "Meshugamh 0.0.x - by Shgck"
PROG_USAGE = "Usage : meshugamh file.fbx\n"

random.seed()


def main():
    print(PROG_TITLE)
        
    if len(sys.argv) != 2:
        print(PROG_USAGE)
        sys.exit()
    
    print("Loading...")
    config = load_config()
    
    input_file = sys.argv[1]
    extension = os.path.splitext(sys.argv[1])[1][1:]
    
    # FBX -> WMO
    if extension.lower() == "fbx":
        
        fbx_importer = FbxImporter(config)
        fbx_importer.load_file(input_file)
        
        if not fbx_importer.scene_is_ok:
            print("FAILED loading {}".format(input_file))
            return
        else:
            print("File loaded : {}".format(input_file))
            
        wmo = fbx_importer.create_wmo()
        
        if not wmo:
            print("WMO creation failed !")
            return
    
        print("Quick semantic check...")
        semantic_check(wmo)
        
        print("Writing...")
        wmo.write_file("test.wmo")
        
        print("Successful conversion !")
        
    # WMO -> FBX
    # Experimental
    elif extension.lower() == "wmo":
        
        fbx_exporter = FbxExporter(config)
        fbx_exporter.build_fbx(input_file)
        
        if not fbx_exporter.scene_is_ok:
            print("FAILED loading {}".format(input_file))
            return
        else:
            print("File loaded : {}".format(input_file))
        
        print("Writing...")
        fbx_exporter.save_file("tests/test.fbx")
        
        print("Successful conversion !")
        
    else:
        print("Please provide a .fbx or a .wmo")
        
    if config["pause_before_exit"] == "yes":
        input("Press Enter to exit...")
    

if __name__ == "__main__":
    main()
    