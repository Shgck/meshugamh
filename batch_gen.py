""" Generate batches. """


def generate_batches(wmo):
    """ Generates all batches for a WMO. """
    for g in wmo.groups:
        generate_group_batches(g)


def generate_group_batches(group):
    """ Generates batches for this WMO group.
        Updates group num_batches info. """
    
    # dummy bounding box, sized to the mesh
    bbox1x = int(group.bbox1[0])
    bbox1y = int(group.bbox1[1])
    bbox1z = int(group.bbox1[2])
    bbox2x = int(group.bbox2[0])
    bbox2y = int(group.bbox2[1])
    bbox2z = int(group.bbox2[2])
    
    # polygon[1] is the material ID
    old_mat = group.polygon_infos[0][1]
    face_count = 0
    batch_face_count = 0
    
    for i, poly in enumerate(group.polygon_infos):
        # if this poly has new mat id, or if it's the last polygon,
        # then we have to create a batch.
        if poly[1] != old_mat or i == len(group.polygon_infos) - 1:
            
            if i == len(group.polygon_infos) - 1:
                batch_face_count += 3
                
            group.batches.append( {
                "bbox1": (bbox1x, bbox1y, bbox1z),
                "bbox2": (bbox2x, bbox2y, bbox2z),
                "first_face": face_count,
                "num_faces": batch_face_count,
                "first_vertex": 0,
                "last_vertex": len(group.vertices) - 1,
                "padding": 0,
                "material_id": old_mat
            } )
            
            face_count += batch_face_count
            batch_face_count = 0
            old_mat = poly[1]
        # end of batch creation
        
        batch_face_count += 3
    
    # all polygons have been parsed
    
    group.batches3 = len(group.batches)
    