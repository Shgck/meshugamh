#######################################################################
# PyWMO - raw import/export of WoW WMO/v17 files
# Copyright (C) 2013 - shgck
#  
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License or
# any later version, or version 2 if explicit permission is given.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#      
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################################

""" Raw import and export of WoW WMO/v17 files.
    Version 0.2a.meshugamh - may be unexpectedly modified

    This module defines the class Wmo you'll probably want to use :  
    --> from shgck_wmo import Wmo
    
    The Wmo groups are stored as defined in the WmoGroup class
    
    It also defines a load of Struct to accelerate binary parsing,
    coming with some functions to make it faster.
    As a user you'll probably not wanna import them.
    
    Please remember the WMO coordinate system is quite special : (X,Z,-Y)
    (in other words : -Y forward, Z up)
    
    Vertices are represented as tuples of 3 floats, same for normals, etc.
    
    Colors are stores as quadruplets. I assume it's (b,g,r,a),
    but it can be wrong. Needs to be investigated.
    
    PyWMO was written by shgck for Modcraft, 
    I hope you guys will have some use of it !
"""

import os.path
from struct import Struct


#struct_i8 = Struct("<b")
struct_u8 = Struct("<B")
#struct_i16 = Struct("<h")
struct_u16 = Struct("<H")
#struct_i32 = Struct("<i")
struct_u32 = Struct("<I")
#struct_f = Struct("<f")

#def _get_i8(f): return struct_i8.unpack(f.read(1))[0]
def _get_u8(f): return struct_u8.unpack(f.read(1))[0]
#def _get_i16(f): return struct_i16.unpack(f.read(2))[0]
def _get_u16(f): return struct_u16.unpack(f.read(2))[0]
#def _get_i32(f): return struct_i32.unpack(f.read(4))[0]
def _get_u32(f): return struct_u32.unpack(f.read(4))[0]
#def _get_float(f): return struct_f.unpack(f.read(4))[0]

# Common structures
struct_id = Struct("<4s")
struct_vec2f = Struct("<ff")
struct_vec3f = Struct("<3f")

def _get_id(f): return struct_id.unpack(f.read(4))[0]
def _get_vec2f(f): return struct_vec2f.unpack(f.read(8))
def _get_vec3f(f): return struct_vec3f.unpack(f.read(12))

# Root file structures
struct_mohd = Struct("<7I4BI6fI")
struct_mat = Struct("<4I4BII4B8I")
struct_gi = Struct("<I6fi")
struct_pinf = Struct("<HH4f")
struct_prel = Struct("<HHhH")
struct_lt = Struct("<8B10f")
struct_dset = Struct("<20s3I")
struct_ddef = Struct("<I8f4B")
struct_fog = Struct("<I7f4Bff4B")

def _get_mohd(f): return struct_mohd.unpack(f.read(64))
def _get_material(f): return struct_mat.unpack(f.read(64))
def _get_group_info(f): return struct_gi.unpack(f.read(32))
def _get_portal_info(f): return struct_pinf.unpack(f.read(20))
def _get_portal_relation(f): return struct_prel.unpack(f.read(8))
def _get_light(f): return struct_lt.unpack(f.read(48))
def _get_dset(f): return struct_dset.unpack(f.read(32))
def _get_ddef(f): return struct_ddef.unpack(f.read(40))
def _get_fog(f): return struct_fog.unpack(f.read(48))

# Group file structures
struct_gp_header = Struct("<3I6f4HI4B4I")
struct_face = Struct("<3H")
struct_batch = Struct("<6hI3HBB")
struct_bsp_node = Struct("<3hHIf")
struct_mliq_header = Struct("<4I3fH")

def _get_mogp(f): return struct_gp_header.unpack(f.read(68))
def _get_face(f): return struct_face.unpack(f.read(6))
def _get_batch(f): return struct_batch.unpack(f.read(24))
def _get_bsp_node(f): return struct_bsp_node.unpack(f.read(16))
def _get_mliq_header(f): return struct_mliq_header.unpack(f.read(30))


def _get_file_size(f):
    """ Gets file size in bytes. Places the cursor at the beginning of f. """
    f.seek(0, 2)
    size = f.tell()
    f.seek(0, 0)
    return size


def _write_chunk(ident, data, out_file):
        """ Write a chunk in binary the old fashion way :
            - the ident, reversed
            - the data size
            - the data """
        out_file.write(ident[::-1].encode())
        out_file.write(struct_u32.pack(len(data)))
        out_file.write(data)


class Wmo(object):
    """ World Model Object : big WoW 3D models, like houses and cities.
    
        This class represents a whole WMO : the root data + its groups.
        You can freely access its variables as it's not meant to be really OOP
        compliant, more aimed on scripting for various purpose.
        It's completely usable in a more complex scope like a viewer or an
        editor if only you're aware of how these models work.
        
        You can create an empty model with Wmo(), then use load_file(path),
        or directly initialize your model with Wmo(path).
        
        About the colors : to make them a bit more easier to modify,
        I store them in quadruplets instead of the classic uint32 format.
        Them being (r,g,b,a) or (b,g,r,a) is not 100% sure if I put "?",
        and would require some testing if needed (and letting me know...)
        
            METHODS :
        load_file(path) : you definitely want to load a file
        write_file(path)
        
            VARIABLES :
        version -- should be 17
        num_materials -- # of MOMT entries
        num_groups -- # of groups attached to this model
        num_portals
        num_lights -- # of MOLT entries
        num_models -- # of doodads imported (MODN entries)
        num_doodads -- # of doodads instanced (MODD entries)
        num_sets -- # of doodad sets (MODS entries)
        ambient_color -- (b,g,r,a) ?
        area_table_id -- ID in WMOAreaTable.dbc (col. 2)
        bbox1 -- bounding-box, inferior corner
        bbox2 -- bounding-box, superior corner
        liquid_type
        
        textures -- list of tuples (offset in MOTX, texture path)
                    It is normal to have empty paths in most models.
        materials -- list of material dicts, as described in the wiki :
                     - flags : general flags
                     - shader
                     - blend_mode
                     - texture1 : texture 1 offset in MOTX
                     - ambient_color : tuple (b,g,r,a) ?
                     - flags_1 : ?
                     - texture2 : texture 1 offset in MOTX (often empty)
                     - diffuse_color : tuple (b,g,r,a) ?
                     - flags_2 : ?
                     - unknown1
                     - unknown2
                     - unknown3
                     there is some data nulled at loading after that.
                     
        group_names -- list of tuples (offset in MOGN, group name str) 
                       Informative only. There are always two null strings
                       at the beginning. The possible null strings at the end
                       are padding and you shouldn't care about them, erase
                       them if you want.
        group_infos -- list of group info dicts :
                       - flags
                       - bbox1 : bounding-box, inferior corner
                       - bbox2 : bounding-box, superior corner
                       - name_index : index of group name in group_names
                                      often -1
                                       
        skybox -- string of the m2 skybox path
                                       
        portal_vertices -- tuples of 4 vertices
        portal_infos -- portal information dicts :
                        - base_vertex_index
                        - num_vertices : always 4 ?
                        - vector : 3 floats, a normal ?
                        - unknown
        portal_relations -- dicts describing relations between two portals :
                            - portal_index
                            - group_index
                            - side : 1 or -1
                            - unknown
                            
        lights -- dicts representing lights as :
                  - flag1 }
                  - flag2 } these flags might be : lightType, type, useAtten, ?
                  - flag3 } they're either 0 or 1.
                  - flag4 }
                  - color : (b,g,r,a) ?
                  - position : vertex
                  - intensity
                  - attenuation_start
                  - attenuation_stop
                  - unknown1
                  - unknown2
                  - unknown3
                  - unknown4
        
        doodad_sets -- dicts for doodad sets :
                       - name : string of the set name
                       - first_def : 1st doodad definition in MODD
                       - num_defs : # of defs for this set in MODD
                       - unknown
        doodad_paths -- tuples for each path : (offset in MODN, str of path)
        doodad_defs -- dicts for doodad definitions :
                       - name_offset : raw offset in MODN 
                       - flags
                       - position : vertex
                       - rotation : quaternion (X,Y,Z,W)
                       - scale
                       - color : (b,g,r,a) ? lighting 
                       
        fogs -- dicts for fog definitions :
                - flags
                - position : vertex
                - small_radius
                - large_radius
                - fog_end
                - fog_start_mult 
                - color1 
                - unknown1
                - unknown2
                - color2
        
        unk_chunks -- when an unknown chunk is parsed, it is added in a raw
                      format in this list as following :
                      - ident : string of the chunk, (ex : "MKOI")
                      - size
                      - data : bytes, raw content of the chunk
        
        groups -- WMO groups. See WmoGroup.

    """
    
    def __init__(self, path=None):
        self.unk_chunks = []
        if path is None:
            self.version = 17
            self.num_materials = 0 
            self.num_groups = 0
            self.num_portals = 0
            self.num_lights = 0
            self.num_models = 0
            self.num_doodads = 0
            self.num_sets = 0
            self.ambient_color = (0, 0, 0, 255)
            self.area_table_id = 0
            self.bbox1 = (0.0, 0.0, 0.0)
            self.bbox2 = (0.0, 0.0, 0.0)
            self.liquid_type = 0
            self.textures = []
            self.materials = []
            self.group_names = [(0, ""), (1, "")]
            self.group_infos = [] 
            self.skybox = ""        
            self.portal_vertices = []
            self.portal_infos = []
            self.portal_relations = []
            self.lights = []
            self.doodad_sets = []
            self.doodad_paths = []
            self.doodad_defs = []
            self.fogs = []
            self.groups = []
        else:
            self.load_file(path)
        
    
    def load_file(self, path):
        """ Loads the WMO stored at path string. Replaces old content. """
        with open(path, "rb") as root_file:
            file_size = _get_file_size(root_file)
            
            while root_file.tell() < file_size:
                chunk_ident = _get_id(root_file)
                chunk_size = _get_u32(root_file)
                
                #print("{} (size : {})".format(chunk_ident, chunk_size))
                
                # MVER : version - it's 17, right ?
                if chunk_ident == b"REVM":
                    self.version = _get_u32(root_file)
                    
                # MOHD : header - some useful stuff
                elif chunk_ident == b"DHOM":
                    mohd = _get_mohd(root_file)
                    self.num_materials = mohd[0]
                    self.num_groups = mohd[1]
                    self.num_portals = mohd[2]
                    self.num_lights = mohd[3]
                    self.num_models = mohd[4]
                    self.num_doodads = mohd[5]
                    self.num_sets = mohd[6]
                    self.ambient_color = (mohd[7], mohd[8], mohd[9], mohd[10]) 
                    self.area_table_id = mohd[11]
                    self.bbox1 = (mohd[12], mohd[13], mohd[14])
                    self.bbox2 = (mohd[15], mohd[16], mohd[17])
                    self.liquid_type = mohd[18]
                    
                # MOTX : textures paths
                elif chunk_ident == b"XTOM":
                    self.textures = []
                    texture_offset = 0
                    running_offset = 0
                    texture = b""
                    while running_offset < chunk_size:
                        c = root_file.read(1)
                        running_offset += 1
                        if c != b"\x00":
                            texture += c
                        else:
                            self.textures.append((texture_offset,
                                                  texture.decode()))
                            texture = b""
                            while running_offset % 4 != 0:  # 4 bytes padding
                                root_file.seek(1, 1)
                                running_offset += 1
                            texture_offset = running_offset

                # MOMT : materials
                elif chunk_ident == b"TMOM":
                    self.materials = [None] * self.num_materials
                    for i in range(self.num_materials):
                        material = {}
                        mat_tuple = _get_material(root_file)
                        material["flags"] = mat_tuple[0]
                        material["shader"] = mat_tuple[1]
                        material["blend_mode"] = mat_tuple[2]
                        material["texture1"] = mat_tuple[3]
                        material["ambient_color"] = (mat_tuple[4],
                                                     mat_tuple[5],
                                                     mat_tuple[6],
                                                     mat_tuple[7])
                        material["flags_1"] = mat_tuple[8]
                        material["texture2"] = mat_tuple[9]
                        material["diffuse_color"] = (mat_tuple[10],
                                                     mat_tuple[11],
                                                     mat_tuple[12],
                                                     mat_tuple[13])
                        material["flags_2"] = mat_tuple[14]
                        material["unknown1"] = mat_tuple[15]
                        material["unknown2"] = mat_tuple[16]
                        material["unknown3"] = mat_tuple[17]
                        self.materials[i] = material
                        
                # MOGN : group names
                elif chunk_ident == b"NGOM":
                    self.group_names = []
                    name_offset = 0
                    running_offset = 0
                    group_name = b""
                    while running_offset < chunk_size:
                        c = root_file.read(1)
                        running_offset += 1
                        if c != b"\x00":
                            group_name += c
                        else:
                            self.group_names.append( (
                                    name_offset,
                                    group_name.decode(errors="replace") ) )
                            group_name = b""
                            name_offset = running_offset
                                
                # MOGI : group informations
                elif chunk_ident == b"IGOM":
                    self.group_infos = [None] * self.num_groups
                    for i in range(self.num_groups):
                        group_info = {}
                        info_tuple = _get_group_info(root_file)
                        group_info["flags"] = info_tuple[0]
                        group_info["bbox1"] = (info_tuple[1],
                                               info_tuple[2],
                                               info_tuple[3])
                        group_info["bbox2"] = (info_tuple[4],
                                               info_tuple[5],
                                               info_tuple[6])
                        group_info["name_index"] = info_tuple[7]
                        self.group_infos[i] = group_info
                        
                # MOSB : skybox model path
                elif chunk_ident == b"BSOM":
                    skybox = b"" 
                    c = root_file.read(1)
                    running_offset = 1
                    while c != b"\x00":
                        skybox += c
                        c = root_file.read(1)
                        running_offset += 1
                    self.skybox = skybox.decode()
                    while running_offset % 4 != 0:  # 4 bytes padding
                        root_file.seek(1, 1)
                        running_offset += 1
                        
                # MOPV : portal vertices, the 3D rectangle of the portal
                #        do not rely on self.num_portals on it...
                elif chunk_ident == b"VPOM":
                    num_portals = chunk_size // 48
                    self.portal_vertices = [None] * num_portals 
                    for i in range(num_portals):
                        self.portal_vertices[i] = ((_get_vec3f(root_file),
                                                    _get_vec3f(root_file),
                                                    _get_vec3f(root_file),
                                                    _get_vec3f(root_file)))
                    root_file.seek((chunk_size - (num_portals * 48)), 1)
                    
                # MOPT : portal infos, linked to portal vertices
                elif chunk_ident == b"TPOM":
                    self.portal_infos = [None] * self.num_portals
                    for i in range(self.num_portals):
                        portal_info = {}
                        info_tuple = _get_portal_info(root_file)
                        portal_info["base_vertex_index"] = info_tuple[0]
                        portal_info["num_vertices"] = info_tuple[1]
                        portal_info["vector"] = (info_tuple[2],
                                                 info_tuple[3],
                                                 info_tuple[4])
                        portal_info["unknown"] = info_tuple[5]
                        self.portal_infos[i] = portal_info
                
                # MOPR : portal relations, using but not linked to the two above
                #        According to relaxok, there isn't always 2 * nPortals.
                elif chunk_ident == b"RPOM":
                    num_relations = int(chunk_size / 8)
                    self.portal_relations = [None] * num_relations
                    for i in range(num_relations):
                        portal_relation = {}
                        relation_tuple = _get_portal_relation(root_file)
                        portal_relation["portal_index"] = relation_tuple[0]
                        portal_relation["group_index"] = relation_tuple[1]
                        portal_relation["side"] = relation_tuple[2]
                        portal_relation["unknown"] = relation_tuple[3]
                        self.portal_relations[i] = portal_relation
                
                # MOVV : visible block vertices
                #elif chunk_ident == b"VVOM":
                #    self.movv = chunk_size
                
                # MOVB : visible block list
                #elif chunk_ident == b"BVOM":
                #    self.movb = chunk_size
                    
                # MOLT : lights
                elif chunk_ident == b"TLOM":
                    self.lights = [None] * self.num_lights
                    for i in range(self.num_lights):
                        light = {}
                        light_tuple = _get_light(root_file)
                        light["flag1"] = light_tuple[0]
                        light["flag2"] = light_tuple[1]
                        light["flag3"] = light_tuple[2]
                        light["flag4"] = light_tuple[3]
                        light["color"] = (light_tuple[4],
                                          light_tuple[5],
                                          light_tuple[6],
                                          light_tuple[7])
                        light["position"] = (light_tuple[8],
                                             light_tuple[9],
                                             light_tuple[10])
                        light["intensity"] = light_tuple[11]
                        light["attenuation_start"] = light_tuple[12]
                        light["attenuation_stop"] = light_tuple[13]
                        light["unknown1"] = light_tuple[14]
                        light["unknown2"] = light_tuple[15]
                        light["unknown3"] = light_tuple[16]
                        light["unknown4"] = light_tuple[17]
                        self.lights[i] = light
                    
                # MODS : doodad sets
                elif chunk_ident == b"SDOM":
                    self.doodad_sets = [None] * self.num_sets
                    for i in range(self.num_sets):
                        doodad_set = {}
                        set_tuple = _get_dset(root_file)
                        doodad_set["name"] = set_tuple[0].decode()
                        doodad_set["first_def"] = set_tuple[1]
                        doodad_set["num_defs"] = set_tuple[2]
                        doodad_set["unknown"] = set_tuple[3]
                        self.doodad_sets[i] = doodad_set
                
                # MODN : doodad paths
                elif chunk_ident == b"NDOM":
                    self.doodad_paths = []
                    running_offset = 0
                    path_offset = 0
                    doodad_path = b""
                    while running_offset < chunk_size:
                        c = root_file.read(1)
                        running_offset += 1
                        if c != b"\x00":
                            doodad_path += c
                        else:
                            self.doodad_paths.append((path_offset,
                                                      doodad_path.decode()))
                            doodad_path = b""
                            while running_offset % 4 != 0:  # 4 bytes padding
                                root_file.seek(1, 1)
                                running_offset += 1
                            path_offset = running_offset
                
                # MODD : doodad definitions
                elif chunk_ident == b"DDOM":
                    num_doodads = int(chunk_size / 40)
                    self.doodad_defs = [None] * num_doodads
                    for i in range(num_doodads):  # local num_doodads
                        doodad = {}
                        doodad_tuple = _get_ddef(root_file)
                        # tricky reading here : name_offset is actually uint24
                        # and the most significant byte is used as flag.
                        doodad["name_offset"] = doodad_tuple[0] & 0xFFFFFF
                        doodad["flags"] = (doodad_tuple[0] & 0xFF000000) >> 24
                        doodad["position"] = (doodad_tuple[1],
                                              doodad_tuple[2],
                                              doodad_tuple[3])
                        doodad["rotation"] = (doodad_tuple[4],
                                              doodad_tuple[5],
                                              doodad_tuple[6],
                                              doodad_tuple[7])
                        doodad["scale"] = doodad_tuple[8]
                        doodad["color"] = (doodad_tuple[9],
                                           doodad_tuple[10],
                                           doodad_tuple[11],
                                           doodad_tuple[12])
                        self.doodad_defs[i] = doodad

                # MFOG : fogs
                elif chunk_ident == b"GOFM":
                    num_fogs = int(chunk_size / 48)
                    self.fogs = [None] * num_fogs
                    for i in range(num_fogs):
                        fog = {}
                        fog_tuple = _get_fog(root_file)
                        fog["flags"] = fog_tuple[0]
                        fog["position"] = (fog_tuple[1],
                                           fog_tuple[2],
                                           fog_tuple[3])
                        fog["small_radius"] = fog_tuple[4]
                        fog["large_radius"] = fog_tuple[5]
                        fog["fog_end"] = fog_tuple[6]
                        fog["fog_start_mult"] = fog_tuple[7] 
                        fog["color1"] = (fog_tuple[8],
                                        fog_tuple[9], 
                                        fog_tuple[10], 
                                        fog_tuple[11]) 
                        fog["unknown1"] = fog_tuple[12]
                        fog["unknown2"] = fog_tuple[13]
                        fog["color2"] = (fog_tuple[14],
                                         fog_tuple[15], 
                                         fog_tuple[16], 
                                         fog_tuple[17])
                        self.fogs[i] = fog

                # Unparsed chunk (no one is left behind I guess)                    
                else:
                    self.unk_chunks.append( {
                        "ident" : chunk_ident.decode()[::-1],
                        "size" : chunk_size,
                        "data" : root_file.read(chunk_size)
                        } )   
            
        # Time to load the group files
        path_noext = os.path.splitext(path)[0]
        self.groups = [None] * self.num_groups
        for i in range(self.num_groups):
            self.groups[i] = WmoGroup("{}_{}.wmo".format(
                                      path_noext, str(i).zfill(3) ) )
            
            
    def write_file(self, path):
        """ Writes the Wmo file with all its group files at specified path. 
            Coding music : Anaal Nathrakh - Vanitas
            Do not complain about bugs. """
        with open(path, "wb") as root_file:
            
            # Version
            _write_chunk( "MVER"
                        , struct_u32.pack(self.version)
                        , root_file )
            
            # Header
            _write_chunk( "MOHD"
                        , struct_mohd.pack( self.num_materials
                                          , self.num_groups
                                          , self.num_portals
                                          , self.num_lights
                                          , self.num_models
                                          , self.num_doodads
                                          , self.num_sets
                                          , self.ambient_color[0]
                                          , self.ambient_color[1]
                                          , self.ambient_color[2]
                                          , self.ambient_color[3]
                                          , self.area_table_id 
                                          , self.bbox1[0]
                                          , self.bbox1[1]
                                          , self.bbox1[2]
                                          , self.bbox2[0]
                                          , self.bbox2[1]
                                          , self.bbox2[2]
                                          , self.liquid_type )
                        , root_file )
    
            # Texture paths
            chunk_data = b""
            for texture_path in self.textures:
                chunk_data += texture_path[1].encode()
                chunk_data += b"\x00"  # zero-terminated string
                while len(chunk_data) % 4 != 0:
                    chunk_data += b"\x00"
            _write_chunk("MOTX", chunk_data, root_file)
            
            # Materials
            chunk_data = b""
            for material in self.materials:
                chunk_data += struct_mat.pack(
                    material["flags"], material["shader"],
                    material["blend_mode"], material["texture1"],
                    material["ambient_color"][0], material["ambient_color"][1],
                    material["ambient_color"][2], material["ambient_color"][3],
                    material["flags_1"], material["texture2"],
                    material["diffuse_color"][0], material["diffuse_color"][1],
                    material["diffuse_color"][2], material["diffuse_color"][3],
                    material["flags_2"],
                    material["unknown1"], 
                    material["unknown2"],
                    material["unknown3"], 
                    0, 0, 0, 0 )
            _write_chunk("MOMT", chunk_data, root_file)
        
            # Group names
            chunk_data = b""
            for group_name in self.group_names:
                chunk_data += group_name[1].encode()
                chunk_data += b"\x00"
            while len(chunk_data) % 4 != 0:
                chunk_data += b"\x00"
            _write_chunk("MOGN", chunk_data, root_file)
                
            # Group infos
            chunk_data = b""
            for group_info in self.group_infos:
                chunk_data += struct_gi.pack( group_info["flags"]
                                            , group_info["bbox1"][0]
                                            , group_info["bbox1"][1]
                                            , group_info["bbox1"][2]
                                            , group_info["bbox2"][0]
                                            , group_info["bbox2"][1]
                                            , group_info["bbox2"][2]
                                            , group_info["name_index"] )
            _write_chunk("MOGI", chunk_data, root_file)
            
            # Skybox model
            chunk_data = self.skybox.encode()
            chunk_data += b"\x00"
            while len(chunk_data) % 4 != 0:
                chunk_data += b"\x00"
            _write_chunk("MOSB", chunk_data, root_file)
            
            # Portal vertices
            chunk_data = b""
            for rectangle in self.portal_vertices:
                chunk_data += struct_vec3f.pack( rectangle[0][0]
                                               , rectangle[0][1]
                                               , rectangle[0][2] )
                chunk_data += struct_vec3f.pack( rectangle[1][0]
                                               , rectangle[1][1]
                                               , rectangle[1][2] )
                chunk_data += struct_vec3f.pack( rectangle[2][0]
                                               , rectangle[2][1]
                                               , rectangle[2][2] )
                chunk_data += struct_vec3f.pack( rectangle[3][0]
                                               , rectangle[3][1]
                                               , rectangle[3][2] )
            _write_chunk("MOPV", chunk_data, root_file)
            
            # Portal infos
            chunk_data = b""
            for portal_info in self.portal_infos:
                chunk_data += struct_pinf.pack(portal_info["base_vertex_index"],
                                               portal_info["num_vertices"],
                                               portal_info["vector"][0],
                                               portal_info["vector"][1],
                                               portal_info["vector"][2],
                                               portal_info["unknown"])
            _write_chunk("MOPT", chunk_data, root_file)
            
            # Portal relations
            chunk_data = b""
            for relation in self.portal_relations:
                chunk_data += struct_prel.pack( relation["portal_index"]
                                              , relation["group_index"]
                                              , relation["side"]
                                              , relation["unknown"] )
            _write_chunk("MOPR", chunk_data, root_file)
            
            # Visible blocks
            _write_chunk("MOVV", b"", root_file)
            _write_chunk("MOVB", b"", root_file)
            
            # Lights
            chunk_data = b""
            for light in self.lights:
                chunk_data += struct_lt.pack(
                    light["flag1"], light["flag2"], 
                    light["flag3"], light["flag4"],
                    light["color"][0], light["color"][1],
                    light["color"][2], light["color"][3],
                    light["position"][0],
                    light["position"][1],
                    light["position"][2],
                    light["intensity"],
                    light["attenuation_start"], light["attenuation_stop"],
                    light["unknown1"], light["unknown2"],
                    light["unknown3"], light["unknown4"] )
            _write_chunk("MOLT", chunk_data, root_file)
            
            # Doodad sets
            chunk_data = b""
            for doodad_set in self.doodad_sets:
                chunk_data += struct_dset.pack( doodad_set["name"].encode()
                                              , doodad_set["first_def"]
                                              , doodad_set["num_defs"]
                                              , doodad_set["unknown"] )
            _write_chunk("MODS", chunk_data, root_file)
            
            # Doodad paths
            chunk_data = b""
            for doodad_path in self.doodad_paths:
                chunk_data += doodad_path[1].encode()
                chunk_data += b"\x00"  # zero-terminated string
                while len(chunk_data) % 4 != 0:
                    chunk_data += b"\x00"
            _write_chunk("MODN", chunk_data, root_file)

            # Doodad definitions
            chunk_data = b""
            for doodad_def in self.doodad_defs:
                chunk_data += struct_ddef.pack(
                    doodad_def["name_offset"] | (doodad_def["flags"] << 24),
                    doodad_def["position"][0], 
                    doodad_def["position"][1], 
                    doodad_def["position"][2], 
                    doodad_def["rotation"][0], doodad_def["rotation"][1],
                    doodad_def["rotation"][2], doodad_def["rotation"][3],
                    doodad_def["scale"],
                    doodad_def["color"][0], doodad_def["color"][1],
                    doodad_def["color"][2], doodad_def["color"][3] )
            _write_chunk("MODD", chunk_data, root_file)
            
            # Fogs
            chunk_data = b""
            for fog in self.fogs:
                chunk_data += struct_fog.pack(
                    fog["flags"],
                    fog["position"][0], fog["position"][1], fog["position"][2], 
                    fog["small_radius"], fog["large_radius"],
                    fog["fog_end"], fog["fog_start_mult"], 
                    fog["color1"][0], fog["color1"][1],
                    fog["color1"][2], fog["color1"][3],
                    fog["unknown1"], fog["unknown2"],
                    fog["color2"][0], fog["color2"][1],
                    fog["color2"][2], fog["color2"][3] )
            _write_chunk("MFOG", chunk_data, root_file)

        path_noext = os.path.splitext(path)[0]
        for i, g in enumerate(self.groups):
            g.write_group("{}_{}.wmo".format(path_noext, str(i).zfill(3)))


class WmoGroup(object):
    """ WMO group file : contains all the raw 3D stuff.
    
        Group files are parts of the whole model, storing the 3D values like
        vertices, faces, texture mapping, batches, a BSP tree, etc.
        Groups are stored in root.groups
        
            METHODS :
        load_group(path) : load file located at path in self.
        write_group(path)
    
            VARIABLES :
        name_offset -- name offset in the root MOGN chunk 
        desc_offset = descriptive name offset in the root MOGN chunk
        flags 
        bbox1 -- bounding box inferior corner
        bbox2 -- bounding box superior corner
        first_portal -- index of the first portal relation used in root MOPR
        num_portals -- # of relation used in MOPR
        batches1 } Sum these 3 values to get the number of batches.
        batches2 } b1 and b2 are stored as uint16, b3 as uint32
        batches3s } At writing, in doubt, write in batches3 only.
        fog_indices -- 4 indices in MFOG : (f1,f2,f3,f4)
        liquid_type
        area_table_id -- ID in WMOAreaTable.dbc (col. 4)
        unknown1
        unknown2

        polygon_infos -- MOPY, list of (flags, material_id)
        
        faces -- MOVI, list of (a, b, c), these indices pointing in vertices
        
        vertices -- MOVT, list of vec3f
        
        normals -- MONR, list of vec3f
        
        texture_maps -- MOTV, list of vec2f (u ,v)

        batches -- list of dicts representing WMO batches (more info in wiki) : 
                   - bbox1 } a box, probably a bounding box 
                   - bbox2 } coded in shorts, not floats !
                   - first_face
                   - num_faces
                   - first_vertex
                   - last_vertex
                   - padding : should be always 0
                   - material_id : index in root MOMT
        
        unk_chunks -- see Wmo variable
        
        The following variables may be None as they're optional chunks.
        - empty list = empty chunk (ident + size 0)
        - None = no chunk at all

        light_refs -- list of references in root MOLT
        doodad_refs -- list of references in root MODD
        
        bsp_nodes -- MOBN, list of dicts representing BSP tree nodes
                     - type : string of the node type.
                              Can be "leaf", "YZ", "XZ", "XY"
                     - child1 } Indices of the children node.
                     - child2 } No child is represented by -1. 
                     - num_faces
                     - first_face
                     - f_dist
        bsp_refs -- MOBR, list of faces references

        vertex_colors -- MOCV, vertex shading colors : (r, g, b, a) quads
        
        liquids -- MLIQ, dict containing some data and two arrays :
                   - x_vertices   
                   - x_vertices  
                   - x_tiles 
                   - y_tiles 
                   - base : vertex, base coordinate of the tiles ?
                   - material_id
                   - height_map : list of (x, y) float tuples
                   - types : list of types, as integers 

    """
    
    def __init__(self, path=None):
        self.light_refs = None
        self.doodad_refs = None
        self.bsp_nodes = None
        self.bsp_refs = None
        self.vertex_colors = None
        self.liquids = None
        self.unk_chunks = []
        if path is None:
            self.version = 17
            self.name_offset = 0
            self.desc_offset = 0
            self.flags = 0
            self.bbox1 = (0.0, 0.0, 0.0)
            self.bbox2 = (0.0, 0.0, 0.0)
            self.first_portal = 0
            self.num_portals = 0
            self.batches1 = 0
            self.batches2 = 0
            self.batches3 = 0
            self.fog_indices = (0, 0, 0, 0)
            self.liquid_type = 15
            self.area_table_id = 0
            self.unknown1 = 0
            self.unknown2 = 0
            self.polygon_infos = []
            self.faces = []
            self.vertices = []
            self.normals = []
            self.texture_maps = []
            self.batches = []
        else:
            self.load_group(path)
    
    
    def load_group(self, path):
        """ Loads the WMO group stored at path string.
            Replaces old content. """
        with open(path, "rb") as group_file:
            file_size = _get_file_size(group_file)
            
            while group_file.tell() < file_size:
                chunk_ident = _get_id(group_file)
                chunk_size = _get_u32(group_file)
                
                #print("{} (size : {})".format(chunk_ident, chunk_size))
                
                # MVER : version
                if chunk_ident == b"REVM":
                    self.version = _get_u32(group_file)

                # MOGP : group chunk, contains all the rest
                elif chunk_ident == b"PGOM":
                    header_tuple = _get_mogp(group_file)
                    self.name_offset = header_tuple[0]
                    self.desc_offset = header_tuple[1]
                    self.flags = header_tuple[2]
                    self.bbox1 = (header_tuple[3],
                                  header_tuple[4],
                                  header_tuple[5])
                    self.bbox2 = (header_tuple[6],
                                  header_tuple[7],
                                  header_tuple[8])
                    self.first_portal = header_tuple[9]
                    self.num_portals = header_tuple[10]
                    self.batches1 = header_tuple[11]
                    self.batches2 = header_tuple[12]
                    self.batches3 = header_tuple[13]
                    self.fog_indices = (header_tuple[14],
                                        header_tuple[15],
                                        header_tuple[16],
                                        header_tuple[17])
                    self.liquid_type = header_tuple[18]
                    self.area_table_id = header_tuple[19]
                    self.unknown1 = header_tuple[20]
                    self.unknown2 = header_tuple[21]
                
                # MOPY : polygon infos
                elif chunk_ident == b"YPOM":
                    num_polygons = int(chunk_size / 2)
                    self.polygon_infos = [None] * num_polygons
                    for i in range(num_polygons):
                        self.polygon_infos[i] = (_get_u8(group_file),
                                                 _get_u8(group_file))
                        
                # MOVI : faces
                elif chunk_ident == b"IVOM":
                    num_faces = int(chunk_size / 6)
                    self.faces = [None] * num_faces
                    for i in range(num_faces):
                        self.faces[i] = _get_face(group_file)
                        
                # MOVT : vertices
                elif chunk_ident == b"TVOM":
                    num_vertices = int(chunk_size / 12)
                    self.vertices = [None] * num_vertices
                    for i in range(num_vertices):
                        self.vertices[i] = _get_vec3f(group_file)
                        
                # MONR : normals
                elif chunk_ident == b"RNOM":
                    num_normals = int(chunk_size / 12)
                    self.normals = [None] * num_normals
                    for i in range(num_normals):
                        self.normals[i] = _get_vec3f(group_file)
                        
                # MOTV : texture mapping
                elif chunk_ident == b"VTOM":
                    num_maps = int(chunk_size / 8)
                    self.texture_maps = [None] * num_maps
                    for i in range(num_maps):
                        self.texture_maps[i] = _get_vec2f(group_file)
                        
                # MOBA : batches
                elif chunk_ident == b"ABOM":
                    num_batches = int(chunk_size / 24)
                    self.batches = [None] * num_batches
                    for i in range(num_batches):
                        batch = {}
                        batch_tuple = _get_batch(group_file)
                        batch["bbox1"] = (batch_tuple[0],
                                          batch_tuple[1],
                                          batch_tuple[2])
                        batch["bbox2"] = (batch_tuple[3],
                                          batch_tuple[4],
                                          batch_tuple[5])
                        batch["first_face"] = batch_tuple[6]
                        batch["num_faces"] = batch_tuple[7]
                        batch["first_vertex"] = batch_tuple[8]
                        batch["last_vertex"] = batch_tuple[9]
                        batch["padding"] = batch_tuple[10]
                        batch["material_id"] = batch_tuple[11]
                        self.batches[i] = batch
                        
                # MOLR : light references
                elif chunk_ident == b"RLOM":
                    num_refs = int(chunk_size / 2)
                    self.light_refs = [None] * num_refs
                    for i in range(num_refs):
                        self.light_refs[i] = _get_u16(group_file)
                
                # MODR : doodad references
                elif chunk_ident == b"RDOM":
                    num_refs = int(chunk_size / 2)
                    self.doodad_refs = [None] * num_refs
                    for i in range(num_refs):
                        self.doodad_refs[i] = _get_u16(group_file)
                        
                # MOBN : BSP tree nodes
                elif chunk_ident == b"NBOM":
                    num_nodes = int(chunk_size / 16)
                    self.bsp_nodes = [None] * num_nodes
                    for i in range(num_nodes):
                        node = {}
                        node_tuple = _get_bsp_node(group_file)
                        if node_tuple[0] == 4:
                            node["type"] = "leaf"
                        elif node_tuple[0] == 0:
                            node["type"] = "YZ"
                        elif node_tuple[0] == 1:
                            node["type"] = "XZ"
                        elif node_tuple[0] == 2:
                            node["type"] = "XY"
                        node["child1"] = node_tuple[1]
                        node["child2"] = node_tuple[2]
                        node["num_faces"] = node_tuple[3]                        
                        node["first_face"] = node_tuple[4]                        
                        node["f_dist"] = node_tuple[5]                        
                        self.bsp_nodes[i] = node
                
                # MOBR : BSP tree references
                elif chunk_ident == b"RBOM":
                    num_refs = int(chunk_size / 2)
                    self.bsp_refs = [None] * num_refs
                    for i in range(num_refs):
                        self.bsp_refs[i] = _get_u16(group_file)
                
                # MOCV : vertex shading
                elif chunk_ident == b"VCOM":
                    num_colors = int(chunk_size / 4)
                    self.vertex_colors = [None] * num_colors
                    for i in range(num_colors):
                        # what order for this components ??
                        b = _get_u8(group_file)
                        g = _get_u8(group_file)
                        r = _get_u8(group_file)
                        a = _get_u8(group_file)
                        self.vertex_colors[i] = (r, g, b, a)
                
                # MLIQ : liquids
                elif chunk_ident == b"QILM":
                    self.liquids = {}
                    liquid_tuple = _get_mliq_header(group_file)
                    self.liquids["x_vertices"] = liquid_tuple[0] 
                    self.liquids["y_vertices"] = liquid_tuple[1]
                    self.liquids["x_tiles"] = liquid_tuple[2]
                    self.liquids["y_tiles"] = liquid_tuple[3]
                    self.liquids["base"] = ( liquid_tuple[4]
                                           , liquid_tuple[5]
                                           , liquid_tuple[6] )
                    self.liquids["material_id"] = liquid_tuple[7]
                    
                    self.liquids["height_map"] = \
                        [None] * liquid_tuple[0] * liquid_tuple[1]
                    for i in range(liquid_tuple[0] * liquid_tuple[1]):
                        self.liquids["height_map"][i] = _get_vec2f(group_file)
                        
                    self.liquids["types"] = \
                        [None] * liquid_tuple[2] * liquid_tuple[3]
                    for i in range(liquid_tuple[2] * liquid_tuple[3]):
                        self.liquids["types"][i] = _get_u8(group_file)

                # Unparsed chunk                    
                else:  
                    self.unk_chunks.append( {
                        "ident" : chunk_ident.decode()[::-1],
                        "size" : chunk_size,
                        "data" : group_file.read(chunk_size)
                        } )                              


    def write_group(self, path):
        """ Writes a Wmo group at specified path.
            Should be called exclusively by a Wmo instance.
            
            As the data is stored as subchunks of MOGP,
            I can't do the same thing as I did for root, so I'll store
            everything in the bytes group_data. """
        with open(path, "wb") as group_file:
            
            # Version
            _write_chunk( "MVER"
                        , struct_u32.pack(self.version)
                        , group_file )
            
            # Group header
            group_data = struct_gp_header.pack(
                 self.name_offset, self.desc_offset,
                 self.flags,
                 self.bbox1[0], self.bbox1[1], self.bbox1[2],
                 self.bbox2[0], self.bbox2[1], self.bbox2[2],
                 self.first_portal, self.num_portals, 
                 self.batches1, self.batches2, self.batches3,
                 self.fog_indices[0], self.fog_indices[1], 
                 self.fog_indices[2], self.fog_indices[3],
                 self.liquid_type,
                 self.area_table_id,
                 self.unknown1, self.unknown2 )
            
            # Polygon infos
            group_data += "MOPY"[::-1].encode()
            group_data += struct_u32.pack(len(self.polygon_infos) * 2) 
            for polygon in self.polygon_infos:
                group_data += struct_u8.pack(polygon[0])
                group_data += struct_u8.pack(polygon[1])
            
            # Faces
            group_data += "MOVI"[::-1].encode()
            group_data += struct_u32.pack(len(self.faces) * 6)
            for face in self.faces:
                group_data += struct_face.pack(face[0], face[1], face[2])
                
            # Vertices
            group_data += "MOVT"[::-1].encode()
            group_data += struct_u32.pack(len(self.vertices) * 12)
            for vertex in self.vertices:
                group_data += struct_vec3f.pack(vertex[0], vertex[1], vertex[2])
                
            # Normals
            group_data += "MONR"[::-1].encode()
            group_data += struct_u32.pack(len(self.normals) * 12)
            for normal in self.normals:
                group_data += struct_vec3f.pack(normal[0], normal[1], normal[2])
                
            # Texture UV maps
            group_data += "MOTV"[::-1].encode()
            group_data += struct_u32.pack(len(self.texture_maps) * 8)
            for uv_map in self.texture_maps:
                # DEBUG In case of unmapped vertex
                if uv_map is None:
                    print("Unmapped vertex, ", end="")
                    group_data += struct_vec2f.pack(0.0, 0.0)
                else:
                    group_data += struct_vec2f.pack(uv_map[0], uv_map[1])
            # DEBUG 
            print("")
                
            # Batches
            group_data += "MOBA"[::-1].encode()
            group_data += struct_u32.pack(len(self.batches) * 24)
            for batch in self.batches:
                group_data += struct_batch.pack(
                    batch["bbox1"][0], batch["bbox1"][1], batch["bbox1"][2],
                    batch["bbox2"][0], batch["bbox2"][1], batch["bbox2"][2],
                    batch["first_face"], batch["num_faces"],
                    batch["first_vertex"], batch["last_vertex"],
                    batch["padding"], batch["material_id"] )
                
            # Light references
            if self.light_refs is not None:
                chunk_data = "MOLR"[::-1].encode()
                chunk_data += struct_u32.pack(len(self.light_refs) * 2)
                for ref in self.light_refs:
                    chunk_data += struct_u16.pack(ref)
                group_data += chunk_data
            
            # Doodad references
            if self.doodad_refs is not None:
                chunk_data = "MODR"[::-1].encode()
                chunk_data += struct_u32.pack(len(self.doodad_refs) * 2)
                for ref in self.doodad_refs:
                    chunk_data += struct_u16.pack(ref)
                group_data += chunk_data
            
            # BSP tree nodes
            if self.bsp_nodes is not None:
                chunk_data = "MOBN"[::-1].encode()
                chunk_data += struct_u32.pack(len(self.bsp_nodes) * 16)
                for node in self.bsp_nodes:
                    node_type = 4
                    if node["type"] == "YZ":
                        node_type = 0
                    elif node["type"] == "XZ":
                        node_type = 1
                    elif node["type"] == "XY":
                        node_type = 2
                    chunk_data += struct_bsp_node.pack(
                        node_type,
                        node["child1"], node["child2"],
                        node["num_faces"], node["first_face"],
                        node["f_dist"] )
                group_data += chunk_data
            
            # BSP tree references
            if self.bsp_refs is not None:
                chunk_data = "MOBR"[::-1].encode()
                chunk_data += struct_u32.pack(len(self.bsp_refs) * 2)
                for ref in self.bsp_refs:
                    chunk_data += struct_u16.pack(ref)
                group_data += chunk_data
            
            # Vertex colors
            if self.vertex_colors is not None:
                chunk_data = "MOCV"[::-1].encode()
                chunk_data += struct_u32.pack(len(self.vertex_colors) * 4)
                for color in self.vertex_colors:  # these are (r,g,b,a)
                    chunk_data += struct_u8.pack(color[2]) # b
                    chunk_data += struct_u8.pack(color[1]) # g
                    chunk_data += struct_u8.pack(color[0]) # r
                    chunk_data += struct_u8.pack(color[3]) # a
                group_data += chunk_data
            
            # Liquids
            if self.liquids is not None:
                chunk_data = "MLIQ"[::-1].encode()
                chunk_data += struct_u32.pack(
                    30 + \
                    len(self.liquids["height_map"]) * 8 + \
                    len(self.liquids["types"]) )
                chunk_data += struct_mliq_header.pack(
                    self.liquids["x_vertices"], self.liquids["y_vertices"],
                    self.liquids["x_tiles"], self.liquids["y_tiles"],
                    self.liquids["base"][0],
                    self.liquids["base"][1],
                    self.liquids["base"][2],
                    self.liquids["material_id"] )
                for i in range(self.liquids["x_vertices"] * \
                               self.liquids["y_vertices"] ):
                    chunk_data += struct_vec2f.pack(
                        self.liquids["height_map"][i][0],
                        self.liquids["height_map"][i][1] )
                for i in range(self.liquids["x_tiles"] * \
                               self.liquids["y_tiles"] ):
                    chunk_data += struct_u8.pack(self.liquids["types"][i])
                group_data += chunk_data
            
            _write_chunk("MOGP", group_data, group_file)
            
            