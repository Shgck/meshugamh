""" Some math utilities for vectors and stuff """

from math import sqrt


def normalize(vector):
    """ Normalize this vector. """
    length = sqrt( vector[0]**2 + vector[1]**2 + vector[2]**2 )
    vector = (vector[0] / length, vector[1] / length, vector[2] / length)
    

def cross_product(v1, v2):
    """ Returns the cross-product of v1 and v2. """
    return ( (v1[1] * v2[2]) - (v1[2] * v2[1]),
              (v1[2] * v2[0]) - (v1[0] * v2[2]),
              (v1[0] * v2[1]) - (v1[1] * v2[0]) )
    
    
def get_face_normal(v1, v2, v3):
    """ Returns a normal for these 3 vertices, therefore it can be 
        considered as a face normal. """
    edge1 = ( v2[0] - v1[0], v2[1] - v1[1], v2[2] - v1[2] )
    edge2 = ( v3[0] - v1[0], v3[1] - v1[1], v3[2] - v1[2] )
    return normalize(cross_product(edge1, edge2))

