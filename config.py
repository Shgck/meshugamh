""" User config module. The config file is now in JSON. """

import json


_default_config = {
    "bsp_leaf_max_size": 2000,
    "fallback_texture": "DUNGEONS\\TEXTURES\\TESTS\\GREY.BLP",
    "textures_path": "",
    "pause_before_exit": "yes"
}


def load_config():
    """ Loads the config file, or create a new one. """
    conf = _default_config
    try:
        with open("config.json", "r") as config_file:
            conf.update( json.load(config_file) )
    except IOError:
        print("Can't load config.json : loading default config and creating config file.")
        _write_config(conf)
    except:
        print("Your config file seems to be corrupt, you should delete it")
        _write_config(conf)
    return conf


def _write_config(config):
    try:
        with open("config.json", "w") as config_file:
            json.dump(config, config_file)
    except IOError:
        print("Can't write config file.")