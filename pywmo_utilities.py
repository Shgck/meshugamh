""" Toolkit to handle some stuff on PyWMO. """

def get_next_group_name_offset(group_names):
    """ From the WMO group names, calculate the offset for a next name. """
    last_name = group_names[-1]
    return last_name[0] + len(last_name[1]) + 1


def get_next_texture_offset(textures):
    """ From the WMO texture paths, calculate the offset for a next path. """
    try:
        last_path = textures[-1]
        next_offset = last_path[0] + len(last_path[1])
        next_offset += 1
        while next_offset % 4 != 0:
            next_offset += 1 
        return next_offset
    except IndexError:
        return 0


def compute_global_bbox(wmo):
    """ Computes the WMO root bounding box. """
    if not wmo.group_infos:
        print("Empty WMO, can't compute bbox.")
        return
    
    lower_x = wmo.group_infos[0]["bbox1"][0]
    lower_y = wmo.group_infos[0]["bbox1"][1]
    lower_z = wmo.group_infos[0]["bbox1"][2]
    upper_x = wmo.group_infos[0]["bbox2"][0]
    upper_y = wmo.group_infos[0]["bbox2"][1]
    upper_z = wmo.group_infos[0]["bbox2"][2]
    
    for group in wmo.group_infos:
        if group["bbox1"][0] < lower_x:
            lower_x = group["bbox1"][0] 
        if group["bbox1"][1] < lower_y:
            lower_y = group["bbox1"][1] 
        if group["bbox1"][2] < lower_z:
            lower_z = group["bbox1"][2] 
        if group["bbox2"][0] > upper_x:
            upper_x = group["bbox2"][0] 
        if group["bbox2"][1] > upper_y:
            upper_y = group["bbox2"][1] 
        if group["bbox2"][2] > upper_z:
            upper_z = group["bbox2"][2]
            
    wmo.bbox1 = (lower_x, lower_y, lower_z)
    wmo.bbox2 = (upper_x, upper_y, upper_z)
    
    
def get_empty_bsp_node():
    """ Returns an empty BSP node, due to their rather specific "default" value,
        it's better like that. """
    return {
        "type": "leaf",
        "child1": -1,
        "child2": -1,
        "num_faces": 0,
        "first_face": 0,
        "f_dist": 0
    }
    
    
def sort_polygons_by_mat(wmo):
    """ For a whole WMO, sort the polygons per their material_id.
    
        This implies sorting also the faces chunk, therefore it should be done
        before generating batches.
    """
    
    for g in wmo.groups:
        sorted_faces = [None] * len(g.faces)
        
        # First we have to mark our polygons
        # to be able to sort the faces the same way.  
        indexed_polygons = list(enumerate(g.polygon_infos))
        
        # Sort by material ID
        indexed_polygons.sort(key = lambda p: p[1][1])
        
        # Set the sorted polygon_infos and faces lists
        g.polygon_infos = [poly for _, poly in indexed_polygons]
        
        for i, (j, _) in enumerate(indexed_polygons):
            sorted_faces[i] = g.faces[j]
        g.faces = sorted_faces


def semantic_check(wmo):
    """ Performs a _basic_ semantic check on the WMO data, in case
        something will very most likely crash ingame. It is not an
        exhaustive check ! """
    
    def warning(message):
        print("WARNING :", message)
    
    # Need a material
    if not wmo.materials:
        warning("The model doesn't have any material !")
    
    # MOHD values    
    if wmo.num_materials != len(wmo.materials):
        warning("Wrong # of materials in header.")
    if wmo.num_groups != len(wmo.groups):
        warning("Wrong # of groups in header.")
    if wmo.num_lights != len(wmo.lights):
        warning("Wrong # of lights in header.")
    if wmo.num_models != len(wmo.doodad_paths):
        warning("Wrong # of doodad paths in header.")
    if wmo.num_sets != len(wmo.doodad_sets):
        warning("Wrong # of doodad sets in header.")
    
    for i, g in enumerate(wmo.groups):
        
        # |vertices| == |normals|
        if len(g.vertices) != len(g.normals):
            warning("Group #{} : number of normals different than "
                    "number of vertices.".format(i))
            
        # |vertices| == |uv|
        if len(g.vertices) != len(g.texture_maps):
            warning("Group #{} : number of UV different than "
                    "number of vertices.".format(i))
            
        # MOCV toggled and |vertex_colors| == |vertices|
        if g.flags & 0x4 and len(g.vertices) != len(g.vertex_colors):
            warning("Group #{} : number of vertex colors different than "
                    "number of vertices.".format(i))
            
        # sum of batches 1, 2, 3 is correct
        if g.batches1 + g.batches2 + g.batches3 != len(g.batches):
            warning("Group #{} : wrong # of batches in the header.".format(i))
            
            
def display_materials(wmo):
    """ Clean/tiny display of materials. """
    print(wmo.num_materials, "materials available :")
    for i, mat in enumerate(wmo.materials):
        name = "<unk>"
        try:
            name = mat["name"]
        except KeyError:
            pass
        
        print("{}: {}, Texture 1 at {}, Texture 2 at {}".format(
               i, name, mat["texture1"], mat["texture2"]) )
        
        
def display_textures(wmo):
    """ Clean/tiny display of textures paths. """
    print("Textures found (without empty entries) :")
    for i, tex in enumerate(wmo.textures):
        name = "<unk>"
        try:
            name = tex[2]
        except IndexError:
            pass
        
        if name != "__filler":
            print("{}: {} at {}, Path : {}".format(
                   i, name, tex[0], tex[1]))

